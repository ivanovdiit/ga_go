package main

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
	"time"
)

type Individual struct {
	size          int
	coefficient   float64
	data          []float64
	survival      float64
	survival_func func([]float64) float64
}

func (individual *Individual) calculateSurvival(chanel chan bool) {
	individual.survival = individual.survival_func(individual.data)
	chanel <- true
}

func (individual *Individual) generate() {
	data := make([]float64, individual.size)
	for i := 0; i < individual.size; i++ {
		data[i] = rand.Float64() * individual.coefficient
	}
	individual.data = data
}

func (individual Individual) reproduction(individual2 Individual, count int) []Individual {

	child := make([]Individual, count)

	for i := 0; i < count; i++ {
		children_data := []float64{}
		for gene_index := range individual.data {
			if rand.Intn(4) >= 2 {
				children_data = append(children_data, individual.data[gene_index])
			} else {
				children_data = append(children_data, individual2.data[gene_index])
			}
		}
		child[i] = Individual{
			individual.size,
			individual.coefficient,
			children_data,
			individual.survival,
			individual.survival_func}
	}

	return child

}

type GA struct {
	individuals    []Individual
	survival       func([]float64) float64
	length         int
	data_size      int
	max_val        float64
	mutation_count int
	child_count    int
}

func (ga *GA) getNewIndividual() Individual {
	individual := Individual{
		ga.data_size,
		ga.max_val,
		[]float64{},
		0,
		ga.survival}
	individual.generate()

	return individual
}

func (ga *GA) init() {
	individuals := make([]Individual, ga.length)
	for i := 0; i < ga.length; i++ {
		individuals[i] = ga.getNewIndividual()
	}
	ga.individuals = individuals
}

func (ga *GA) reproduction() {
	parents := ga.individuals
	for _, individual := range parents {
		for _, individual2 := range parents {
			child := individual.reproduction(individual2, ga.child_count)
			for _, child := range child {
				ga.individuals = append(ga.individuals, child)
			}
		}
	}
}

func (ga *GA) mutation() {
	for i := 0; i < ga.mutation_count; i++ {
		ga.individuals = append(ga.individuals, ga.getNewIndividual())
	}
}

func (ga *GA) selection() {
	chanel := make(chan bool, cap(ga.individuals))
	for i := range ga.individuals {
		go ga.individuals[i].calculateSurvival(chanel)
	}

	for range ga.individuals {
		<-chanel
	}	

	sort.Sort(ga)
	ga.individuals = ga.individuals[0:ga.length]
}

func (ga GA) Len() int {
	return len(ga.individuals)
}

func (ga GA) Swap(i, j int) {
	ga.individuals[i], ga.individuals[j] = ga.individuals[j], ga.individuals[i]
}

func (ga GA) Less(i, j int) bool {
	return math.Abs(ga.individuals[i].survival) < math.Abs(ga.individuals[j].survival)
}

func (ga GA) PrintResult() {
	for i, individual := range ga.individuals {
		fmt.Println(i)
		fmt.Println("Survival: ", individual.survival)
		fmt.Println(individual.data)
	}
}

func survival_function(data []float64) float64 {
	return 500 - (5*data[0] + 25*data[1] + data[2])
}

func main() {
	rand.Seed(time.Now().Unix())
	ga := GA{
		[]Individual{},
		survival_function,
		100,
		3,
		500,
		25,
		1}

	ga.init()

	for i := 0; i < 1000; i++ {
		ga.mutation()
		ga.reproduction()
		ga.selection()
	}
	ga.PrintResult()
}
